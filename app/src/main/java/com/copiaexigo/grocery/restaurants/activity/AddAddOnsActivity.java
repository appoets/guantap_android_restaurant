package com.copiaexigo.grocery.restaurants.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.helper.ConnectionHelper;
import com.copiaexigo.grocery.restaurants.helper.CustomDialog;
import com.copiaexigo.grocery.restaurants.helper.GlobalData;
import com.copiaexigo.grocery.restaurants.model.Addon;
import com.copiaexigo.grocery.restaurants.model.AddonsGroups;
import com.copiaexigo.grocery.restaurants.model.ServerError;
import com.copiaexigo.grocery.restaurants.network.ApiClient;
import com.copiaexigo.grocery.restaurants.network.ApiInterface;
import com.copiaexigo.grocery.restaurants.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddOnsActivity extends AppCompatActivity {


    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.et_addons_name)
    EditText etAddonsName;
    @BindView(R.id.save_btn)
    Button saveBtn;
    @BindView(R.id.spinnerAddons)
    MaterialSpinner spinnerAddons;

    private Context context = AddAddOnsActivity.this;
    private Activity activity;
    private CustomDialog customDialog;
    private final ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private String TAG = "AddAddOnsActivity";
    private Integer itemID = 0;
    private boolean isInternet;
    List<AddonsGroups> addonsGroups = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_add_ons);
        ButterKnife.bind(this);
        if (getIntent().getBooleanExtra("is_update", false)) {
            title.setText(getString(R.string.update_add_ons));
            Addon addon = GlobalData.selectedAddon;
            if (addon != null && addon.getName() != null &&
                    !addon.getName().equalsIgnoreCase("null") && addon.getName().length() > 0) {
                etAddonsName.setText(addon.getName());
            }
        } else
            title.setText(getString(R.string.create_add_ons));
        backImg.setVisibility(View.VISIBLE);
        context = AddAddOnsActivity.this;
        activity = AddAddOnsActivity.this;
        ConnectionHelper connectionHelper = new ConnectionHelper(context);
        isInternet = connectionHelper.isConnectingToInternet();
        customDialog = new CustomDialog(context);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getAddonsGroup();
    }

    private void getAddonsGroup() {
        addonsGroups.clear();
        customDialog.show();
        Call<List<AddonsGroups>> call = apiInterface.getAddonsGroups();
        call.enqueue(new Callback<List<AddonsGroups>>() {
            @Override
            public void onResponse(Call<List<AddonsGroups>> call, Response<List<AddonsGroups>> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            addonsGroups.add(new AddonsGroups(0, "Select", 0, 0, 0));
                            addonsGroups.addAll(response.body());
                            spinnerAddons.setItems(addonsGroups);
                            if (getIntent().getBooleanExtra("is_update", false)) {
                                if (GlobalData.selectedAddon.getAddongroup() != null) {
                                    AddonsGroups addonsGroups1 = GlobalData.selectedAddon.getAddongroup();
                                    int index = addonsGroups.indexOf(addonsGroups1);
                                    if (index != -1) {
                                        spinnerAddons.setSelectedIndex(index);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    Gson gson = new Gson();
                    try {
                        ServerError serverError = gson.fromJson(response.errorBody().charStream(), ServerError.class);
                        Utils.displayMessage(activity, serverError.getError());
                    } catch (JsonSyntaxException e) {
                        Utils.displayMessage(activity, getString(R.string.something_went_wrong));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<AddonsGroups>> call, Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, getString(R.string.something_went_wrong));
            }
        });

    }

    @OnClick({R.id.back_img, R.id.save_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                onBackPressed();
                break;
            case R.id.save_btn:
                if (!addonsGroups.isEmpty()) {
                    itemID = addonsGroups.get(spinnerAddons.getSelectedIndex()).getId();
                    Log.d(TAG, "ID______________: " + itemID);
                }
                String name = etAddonsName.getText().toString();
                if (!name.isEmpty()) {
                    if (itemID != 0) {
                        addAddOns(name);
                    } else {
                        Utils.displayMessage(activity, getString(R.string.please_select_addon_group));
                    }
                } else
                    Utils.displayMessage(activity, getResources().getString(R.string.please_enter_addons_name));
                break;
        }
    }

    private void addAddOns(String name) {
        customDialog.show();
        Call<Addon> call;
        if (getIntent().getBooleanExtra("is_update", false))
            call = apiInterface.updateAddon(GlobalData.selectedAddon.getId(), name,itemID);
        else
            call = apiInterface.addAddon(name, itemID);

        call.enqueue(new Callback<Addon>() {
            @Override
            public void onResponse(@NonNull Call<Addon> call, @NonNull Response<Addon> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    if (getIntent().getBooleanExtra("is_update", false))
                        Toast.makeText(context, getResources().getString(R.string.addons_updated_successfully), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(context, getResources().getString(R.string.addons_added_successfully), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Gson gson = new Gson();
                    try {
                        ServerError serverError = gson.fromJson(response.errorBody().charStream(), ServerError.class);
                        Utils.displayMessage(activity, serverError.getError());
                    } catch (JsonSyntaxException e) {
                        Utils.displayMessage(activity, getString(R.string.something_went_wrong));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Addon> call, @NonNull Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, getString(R.string.something_went_wrong));
            }
        });
    }

}
