package com.copiaexigo.grocery.restaurants.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.helper.ConnectionHelper;
import com.copiaexigo.grocery.restaurants.helper.CustomDialog;
import com.copiaexigo.grocery.restaurants.helper.SharedHelper;
import com.copiaexigo.grocery.restaurants.imagecompressor.Compressor;
import com.copiaexigo.grocery.restaurants.messages.ProductMessage;
import com.copiaexigo.grocery.restaurants.model.addonsdata.ProductAddonGroup;
import com.copiaexigo.grocery.restaurants.model.addonsdata.ProductAddonsList;
import com.copiaexigo.grocery.restaurants.model.addonsdata.UpdatedAddons;
import com.copiaexigo.grocery.restaurants.model.product.ProductResponse;
import com.copiaexigo.grocery.restaurants.network.ApiClient;
import com.copiaexigo.grocery.restaurants.network.ApiInterface;
import com.copiaexigo.grocery.restaurants.utils.Constants;
import com.copiaexigo.grocery.restaurants.utils.Utils;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductAddOnActivity extends AppCompatActivity {
    private static ProductMessage message;
    private final int ADD_ON_REQ_CODE = 1000;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back_img)
    ImageView imgBack;
    @BindView(R.id.tvAddons)
    TextView tvAddons;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.etPrice)
    EditText etPrice;
    @BindView(R.id.etDiscount)
    EditText etDiscount;
    @BindView(R.id.spinnerDiscountType)
    MaterialSpinner spinnerDiscountType;

    ArrayList<String> lstDiscountType = new ArrayList<String>();
    String strDiscountType = "";
    String strProductPrice = "";
    String strProductDiscount = "";
    ConnectionHelper connectionHelper;
    CustomDialog customDialog;
    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    ArrayList<UpdatedAddons> addOnSelectedList = new ArrayList<>();
    ArrayList<ProductAddonGroup> addOnReceivedList = new ArrayList<>();
    ProductResponse productResponse;


    public static void setMessage(ProductMessage message) {
        ProductAddOnActivity.message = message;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add_on);
        ButterKnife.bind(this);
        setUp();
    }


    private void setUp() {
        connectionHelper = new ConnectionHelper(this);
        customDialog = new CustomDialog(this);
        title.setText(getString(R.string.add_product));
        imgBack.setVisibility(View.VISIBLE);
        setDiscountTypeSpinner();
        Bundle bundle = getIntent().getExtras();
        String currency = SharedHelper.getKey(this, Constants.PREF.CURRENCY);
        tvPrice.setText("Price (" + currency + ")");

        if (bundle != null) {
            title.setText(R.string.edit_product);
            productResponse = bundle.getParcelable("product_data");
            if (productResponse.getPrices() != null) {
                etPrice.setText(productResponse.getPrices().getPrice() + "");
            }
            if (productResponse.getPrices().getDiscountType().equalsIgnoreCase("percentage")) {
                spinnerDiscountType.setSelectedIndex(1);
                strDiscountType = "Percentage";
            } else if (productResponse.getPrices().getDiscountType().equalsIgnoreCase("amount")) {
                spinnerDiscountType.setSelectedIndex(2);
                strDiscountType = "Amount";
            }

            if (productResponse.getPrices() != null) {
                etDiscount.setText(productResponse.getPrices().getDiscount() + "");
            }

            if (!addOnReceivedList.isEmpty()) {
                addOnReceivedList.clear();
            }

            if (productResponse.getAddonGroup().size() > 0) {
                addOnReceivedList.addAll(productResponse.getAddonGroup());
                getAddonData(addOnReceivedList);
            }


            /*if (productResponse.getAddons().size() > 0) {
                String addOnNames = "";

                for (int i = 0; i < productResponse.getAddons().size(); i++) {
                    if (productResponse.getAddons().get(i).getAddon() != null) {
                        if (i == 0) {
                            addOnNames = productResponse.getAddons().get(i).getAddon().getName();
                        } else {
                            addOnNames = addOnNames + "," + productResponse.getAddons().get(i).getAddon().getName();
                        }
                    }
                }
                tvAddons.setText(addOnNames);
            }*/
        }
    }

    private void getUpdatedAddons(ArrayList<UpdatedAddons> arrayList) {
        String strSeparator = ", ";
        StringBuilder carAddons = new StringBuilder();
        for (UpdatedAddons data : arrayList) {
            carAddons.append(data.getAddonName());
            carAddons.append(strSeparator);
        }
        String afterAppending = carAddons.toString();
        tvAddons.setText(afterAppending.substring(0, afterAppending.length() - strSeparator.length()));
    }

    private void getAddonData(ArrayList<ProductAddonGroup> arrayList) {
        String strSeparator = ", ";
        StringBuilder carAddons = new StringBuilder();
        for (ProductAddonGroup data : arrayList) {
            for (ProductAddonsList group : data.getAddon()) {
                carAddons.append(group.getName());
                carAddons.append(strSeparator);
            }
        }
        String afterAppending = carAddons.toString();
        tvAddons.setText(afterAppending.substring(0, afterAppending.length() - strSeparator.length()));
    }


    private void setDiscountTypeSpinner() {
        lstDiscountType.add("Select Discount Type");
        lstDiscountType.add("Percentage");
        lstDiscountType.add("Amount");
        spinnerDiscountType.setItems(lstDiscountType);
        spinnerDiscountType.setOnItemSelectedListener((view, position, id, item) -> strDiscountType = lstDiscountType.get(position));
    }

    @OnClick({R.id.btnSave, R.id.ivAddOn, R.id.back_img})
    public void SubmitNext(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if (validateInput()) {
                    addProduct();
                }
                break;

            case R.id.back_img:
                onBackPressed();
                break;

            case R.id.ivAddOn:
                Intent intent = new Intent(this, ProductAddOnListActivity.class);
                if (addOnReceivedList.size() > 0) {
                    intent.putParcelableArrayListExtra("addon", addOnReceivedList);
                }
                startActivityForResult(intent, ADD_ON_REQ_CODE);
                break;
        }
    }

    private void addProduct() {
        customDialog.show();
        String shop_id = SharedHelper.getKey(this, Constants.PREF.PROFILE_ID);
        HashMap<String, RequestBody> params = new HashMap<>();

        if (message.getStrProductOrder().equals("")) {
            message.setStrProductOrder("0");
        }

        params.put("name", toRequestBody(message.getStrProductName()));
        params.put("description", toRequestBody(message.getStrProductDescription()));
        params.put("category", toRequestBody(message.getStrProductCategory()));
        params.put("price", toRequestBody(strProductPrice));
        params.put("product_position", toRequestBody(message.getStrProductOrder()));
        params.put("shop", toRequestBody(shop_id));
        String featured = (message.getFeaturedImageFile() != null) ? "1" : "0";
        params.put("discount", toRequestBody(strProductDiscount));
        params.put("discount_type", toRequestBody(strDiscountType));
        params.put("status", toRequestBody(message.getStrProductStatus()));
        params.put("cuisine_id", toRequestBody(message.getStrCuisineId()));
        params.put("food_type", toRequestBody(message.getStrSelectedFoodType()));
        params.put("featured_position", toRequestBody("1"));

        if (!addOnSelectedList.isEmpty()) {
            for (int i = 0; i < addOnSelectedList.size(); i++) {
                params.put("addon_group_id[" + addOnSelectedList.get(i).getAddonGroupId() + "]", toRequestBody(addOnSelectedList.get(i).getAddonGroupId() + ""));
                params.put("addons[" + addOnSelectedList.get(i).getAddonId() + "]", toRequestBody(addOnSelectedList.get(i).getAddonId() + ""));
                params.put("addons_price[" + addOnSelectedList.get(i).getAddonId() + "]", toRequestBody(addOnSelectedList.get(i).getPrice()));
            }
        } else if (!addOnReceivedList.isEmpty()) {
            for (int i = 0; i < addOnReceivedList.size(); i++) {
                params.put("addon_group_id[" + addOnReceivedList.get(i).getId() + "]", toRequestBody(addOnReceivedList.get(i).getId() + ""));
                if (addOnReceivedList.get(i).getAddon() != null) {
                    for (ProductAddonsList addonsList : addOnReceivedList.get(i).getAddon()) {
                        params.put("addons[" + addonsList.getId() + "]", toRequestBody(addonsList.getId() + ""));
                        params.put("addons_price[" + addonsList.getId() + "]", toRequestBody("" + addonsList.getAddonproducts().getPrice()));
                    }
                }
            }
        }


        if (productResponse != null) {
            params.put("featured_position", toRequestBody(featured));
            params.put("_method", toRequestBody("PATCH"));
        } else {
            params.put("featured", toRequestBody(featured));
        }

        MultipartBody.Part filePart1 = null;
        if (message.getProductImageFile() != null) {
            try {
                File compressToFile = new Compressor(this).compressToFile(message.getProductImageFile());
                filePart1 = MultipartBody.Part.createFormData("avatar[]", compressToFile.getName(), RequestBody.create(compressToFile, MediaType.parse("image/*")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        MultipartBody.Part filePart2 = null;
        /*if (message.getFeaturedImageFile() != null)
            filePart2 = MultipartBody.Part.createFormData("featured_image", message.getFeaturedImageFile().getName(),
                    RequestBody.create(MediaType.parse("image/*"), message.getFeaturedImageFile()));*/

        if (message.getFeaturedImageFile() != null) {
            try {
                File compressToFile = new Compressor(this).compressToFile(message.getFeaturedImageFile());
                filePart2 = MultipartBody.Part.createFormData("featured_image", compressToFile.getName(), RequestBody.create(compressToFile, MediaType.parse("image/*")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Call<ProductResponse> call = null;

        if (productResponse != null) {
            int product_id = productResponse.getId();
            call = apiInterface.updateProduct(product_id, params, filePart1, filePart2);
        } else {
            call = apiInterface.addProduct(params, filePart1, filePart2);
        }

        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    redirectToProductList();
                } else {
                    Utils.displayMessage(ProductAddOnActivity.this, "failed");
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(ProductAddOnActivity.this, t.toString());
            }
        });
    }


    private RequestBody toRequestBody(String value) {
        return RequestBody.create(value, MediaType.parse("text/plain"));
    }

    private void redirectToProductList() {
        Utils.displayMessage(ProductAddOnActivity.this, getString(R.string.product_added_successfully));
        new Handler(Looper.myLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                finishAffinity();
            }
        }, 1000);
    }

    private boolean validateInput() {
        strProductPrice = etPrice.getText().toString().trim();
        strProductDiscount = etDiscount.getText().toString().trim();

        if (strProductPrice.isEmpty()) {
            Utils.displayMessage(this, getString(R.string.please_enter_price));
            return false;
        } else if (strDiscountType.isEmpty()) {
            Utils.displayMessage(this, getString(R.string.please_select_discount_type));
            return false;
        } else if (strProductDiscount.isEmpty()) {
            Utils.displayMessage(this, getString(R.string.please_enter_discount_amount));
            return false;
        }


        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (requestCode == ADD_ON_REQ_CODE && resultCode == RESULT_OK) {
            if (!addOnList.isEmpty()) {
                addOnList.clear();
            }
            addOnList = data.getParcelableArrayListExtra("addon");
            String addOns = "";
            if (addOnList.size() > 0) {
                for (int i = 0; i < addOnList.size(); i++) {
                    if (i == 0) {
                        addOns = addOnList.get(i).getName();
                    } else {
                        addOns = addOns + "," + addOnList.get(i).getName();
                    }
                }
            }
            tvAddons.setText(addOns);
        }*/
        if (requestCode == ADD_ON_REQ_CODE && resultCode == RESULT_OK) {
            if (!addOnSelectedList.isEmpty()) {
                addOnSelectedList.clear();
            }
            Bundle bundle = data.getExtras();
            addOnSelectedList = bundle.getParcelableArrayList("addon");
            getUpdatedAddons(addOnSelectedList);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        message = null;
    }
}
