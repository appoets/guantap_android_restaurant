package com.copiaexigo.grocery.restaurants.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.adapter.AddOnsGroupAdapter;
import com.copiaexigo.grocery.restaurants.helper.ConnectionHelper;
import com.copiaexigo.grocery.restaurants.helper.CustomDialog;
import com.copiaexigo.grocery.restaurants.helper.GlobalData;
import com.copiaexigo.grocery.restaurants.model.AddonsGroups;
import com.copiaexigo.grocery.restaurants.model.ServerError;
import com.copiaexigo.grocery.restaurants.network.ApiClient;
import com.copiaexigo.grocery.restaurants.network.ApiInterface;
import com.copiaexigo.grocery.restaurants.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddOnsGroupsActivity extends AppCompatActivity implements AddOnsGroupAdapter.AddOnGroupClick {
    private final ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private final String TAG = "AddOnsGroupsActivity";
    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.add_ons_rv)
    RecyclerView addOnsRv;
    @BindView(R.id.add_add_ons_btn)
    Button addAddOnsBtn;
    @BindView(R.id.llNoRecords)
    LinearLayout llNoRecords;
    private Context context;
    private Activity activity;
    private CustomDialog customDialog;
    private boolean isInternet;
    private AddOnsGroupAdapter addOnsGroupAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addon_group_list_activity);
        ButterKnife.bind(this);
        title.setText(getString(R.string.add_ons_group_list));
        backImg.setVisibility(View.VISIBLE);
        context = AddOnsGroupsActivity.this;
        activity = AddOnsGroupsActivity.this;
        ConnectionHelper connectionHelper = new ConnectionHelper(context);
        isInternet = connectionHelper.isConnectingToInternet();
        customDialog = new CustomDialog(context);
        setGroupAdapter();
    }

    private void setGroupAdapter() {
        addOnsGroupAdapter = new AddOnsGroupAdapter(this);
        addOnsRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        addOnsRv.setHasFixedSize(true);
        addOnsRv.setAdapter(addOnsGroupAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isInternet)
            getAddonsGroup();
        else
            Utils.displayMessage(activity, getString(R.string.oops_no_internet));

    }

    private void getAddonsGroup() {
        customDialog.show();
        Call<List<AddonsGroups>> call = apiInterface.getAddonsGroups();
        call.enqueue(new Callback<List<AddonsGroups>>() {
            @Override
            public void onResponse(Call<List<AddonsGroups>> call, Response<List<AddonsGroups>> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            llNoRecords.setVisibility(View.GONE);
                            addOnsRv.setVisibility(View.VISIBLE);
                        } else {
                            llNoRecords.setVisibility(View.VISIBLE);
                            addOnsRv.setVisibility(View.GONE);
                        }
                        addOnsGroupAdapter.setItemList(response.body());
                    }
                } else {
                    Gson gson = new Gson();
                    try {
                        ServerError serverError = gson.fromJson(response.errorBody().charStream(), ServerError.class);
                        Utils.displayMessage(activity, serverError.getError());
                    } catch (JsonSyntaxException e) {
                        Utils.displayMessage(activity, getString(R.string.something_went_wrong));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<AddonsGroups>> call, Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, getString(R.string.something_went_wrong));
            }
        });

    }

    @OnClick({R.id.back_img, R.id.add_add_ons_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                onBackPressed();
                break;
            case R.id.add_add_ons_btn:
                startActivity(new Intent(context, AddAddOnsGroupActivity.class));
                break;
        }
    }

    public void deleteAddonGroup(final AddonsGroups addon) {
        customDialog.show();
        Call<Object> call = apiInterface.deleteAddonsGroup(addon.getId());
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull Response<Object> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    Utils.displayMessage(activity, getString(R.string.deleted_success));
                    getAddonsGroup();
                } else {
                    Gson gson = new Gson();
                    try {
                        ServerError serverError = gson.fromJson(response.errorBody().charStream(), ServerError.class);
                        Utils.displayMessage(activity, serverError.getError());
                    } catch (JsonSyntaxException e) {
                        Utils.displayMessage(activity, getString(R.string.something_went_wrong));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(activity, getString(R.string.something_went_wrong));
            }
        });
    }

    @Override
    public void itemClick(AddonsGroups addon) {
        GlobalData.selectedAddonGroup = addon;
        startActivity(new Intent(context, AddAddOnsGroupActivity.class).putExtra("is_update", true));
    }

    @Override
    public void itemDelete(AddonsGroups addon) {
        deleteAddonGroup(addon);
    }
}
