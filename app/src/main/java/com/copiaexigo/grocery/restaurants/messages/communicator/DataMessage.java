package com.copiaexigo.grocery.restaurants.messages.communicator;

public interface DataMessage<T> {

    void onReceiveData(T t);
}
