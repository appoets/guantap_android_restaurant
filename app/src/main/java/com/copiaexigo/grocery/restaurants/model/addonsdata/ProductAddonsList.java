package com.copiaexigo.grocery.restaurants.model.addonsdata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ProductAddonsList implements Parcelable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("shop_id")
    private Integer shopId;
    @SerializedName("deleted_at")
    private Object deletedAt;
    @SerializedName("addon_group_id")
    private Integer addonGroupId;
    @SerializedName("addonproducts")
    private ProductAddon addonproducts;
    private String price = "0";
    private boolean isChecked;


    protected ProductAddonsList(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        price = in.readString();
        if (in.readByte() == 0) {
            shopId = null;
        } else {
            shopId = in.readInt();
        }
        if (in.readByte() == 0) {
            addonGroupId = null;
        } else {
            addonGroupId = in.readInt();
        }
        addonproducts = (ProductAddon) in.readValue(ProductAddon.class.getClassLoader());
        isChecked = in.readByte() != 0x00;
    }

    public static final Creator<ProductAddonsList> CREATOR = new Creator<ProductAddonsList>() {
        @Override
        public ProductAddonsList createFromParcel(Parcel in) {
            return new ProductAddonsList(in);
        }

        @Override
        public ProductAddonsList[] newArray(int size) {
            return new ProductAddonsList[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getAddonGroupId() {
        return addonGroupId;
    }

    public void setAddonGroupId(Integer addonGroupId) {
        this.addonGroupId = addonGroupId;
    }

    public ProductAddon getAddonproducts() {
        return addonproducts;
    }

    public void setAddonproducts(ProductAddon addonproducts) {
        this.addonproducts = addonproducts;
    }

    public Boolean getIsCheck() {
        return isChecked;
    }

    public void setChecked(Boolean itemCheck) {
        isChecked = itemCheck;
    }

    public String getPrice() {
        if (price == null)
            price = "";
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(name);
        dest.writeString(price);
        if (shopId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(shopId);
        }
        if (addonGroupId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addonGroupId);
        }
        dest.writeValue(addonproducts);
        dest.writeByte((byte) (isChecked ? 0x01 : 0x00));
    }
}
