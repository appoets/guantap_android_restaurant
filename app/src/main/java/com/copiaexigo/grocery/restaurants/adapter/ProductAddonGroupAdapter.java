package com.copiaexigo.grocery.restaurants.adapter;

import android.app.Activity;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.model.addonsdata.ProductAddonGroup;
import com.copiaexigo.grocery.restaurants.model.addonsdata.ProductAddonsList;
import com.copiaexigo.grocery.restaurants.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProductAddonGroupAdapter extends SectionedRecyclerViewAdapter<ProductAddonGroupAdapter.SizeVH> {

    private static final String TAG = "ProductAddonGroupAdapte";
    public ArrayList<ProductAddonGroup> itemList = new ArrayList<>();
    ArrayList<ProductAddonGroup> addonGroupArrayList = new ArrayList<>();

    Activity mContext;

    public ProductAddonGroupAdapter(Activity mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getSectionCount() {
        return itemList.size();
    }

    @Override
    public int getItemCount(int section) {
        return itemList.get(section).getAddon().size();
    }

    @Override
    public void onBindHeaderViewHolder(SizeVH sizeVH, int position) {
        sizeVH.bindHeaderData(itemList.get(position));
        sizeVH.ctvAddOnGroupName.setChecked(itemList.get(position).isChecked());
        sizeVH.ctvAddOnGroupName.setOnClickListener(v -> {
            boolean isCheck = false;
            if (!itemList.get(position).isChecked()) {
                isCheck =true;
            }
            itemList.get(position).setChecked(isCheck);
            for (int i = 0; i < itemList.get(position).getAddon().size(); i++) {
                itemList.get(position).getAddon().get(i).setChecked(isCheck);
            }
            notifyDataSetChanged();
        });
    }

    @Override
    public void onBindViewHolder(SizeVH sizeVH, final int section, final int relativePosition, int absolutePosition) {
        final ProductAddonsList data = itemList.get(section).getAddon().get(relativePosition);
        sizeVH.ctvAddOnName.setOnClickListener(v -> {
            itemList.get(section).getAddon().get(relativePosition).setChecked(!data.getIsCheck());
            notifyDataSetChanged();
        });
        sizeVH.headerPosition = section;
        sizeVH.itemPosition = relativePosition;

        sizeVH.etPrice.setTag(relativePosition);
        sizeVH.ctvAddOnName.setTag(relativePosition);

        sizeVH.ctvAddOnName.setText(data.getName());
        sizeVH.ctvAddOnName.setChecked(data.getIsCheck());
        final String prefix = sizeVH.itemView.getContext().getResources().getString(R.string.currency_value);
        if (!data.getPrice().isEmpty() && !data.getPrice().startsWith(prefix)) {
            sizeVH.etPrice.setText(String.format("%s%s", prefix, data.getPrice()));
        } else if (!data.getPrice().isEmpty() && data.getPrice().startsWith(prefix)) {
            sizeVH.etPrice.setText(data.getPrice());
        } else {
            sizeVH.etPrice.setText(prefix);
        }
        Selection.setSelection(sizeVH.etPrice.getText(), sizeVH.etPrice.getText().length());

    }


    @NonNull
    @Override
    public SizeVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_addon_header, parent, false);
                return new SizeVH(v, true);
            case VIEW_TYPE_ITEM:
            default:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_addons_list_item, parent, false);
                return new SizeVH(v, false);
        }
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<ProductAddonGroup> getItemList() {
        return itemList;
    }


    public ArrayList<ProductAddonGroup> getSelectAddOnList() {
        return itemList;
    }

    public boolean verifyPrice() {
        addonGroupArrayList.clear();
        final String prefix = mContext.getResources().getString(R.string.currency_value);
        for (int i = 0; i < itemList.size(); i++) {
            ProductAddonGroup item = itemList.get(i);
            for (ProductAddonsList addonsList : item.getAddon()) {
                if (addonsList.getIsCheck()) {
                    if (addonsList.getPrice().replace(prefix, "").isEmpty()) {
                        Utils.displayMessage(mContext, mContext.getResources().getString(R.string.please_enter_price));
                        return false;
                    }

                    addonGroupArrayList.add(item);
                }
            }
        }
        return true;
    }


    public void setItemList(ArrayList<ProductAddonGroup> items) {
        itemList.addAll(items);
    }

    class SizeVH extends RecyclerView.ViewHolder {
        private EditText etPrice;
        private CheckedTextView ctvAddOnName;
        private CheckedTextView ctvAddOnGroupName;
        private Integer headerPosition = 0;
        private Integer itemPosition = 0;

        public SizeVH(@NonNull View itemView, boolean isHeader) {
            super(itemView);
            if (isHeader) {
                ctvAddOnGroupName = itemView.findViewById(R.id.ctvAddOnGroupName);
            } else {
                etPrice = itemView.findViewById(R.id.etPrice);
                ctvAddOnName = itemView.findViewById(R.id.ctvAddOnName);
                etPrice.addTextChangedListener(new GenericTextWatcher(etPrice));
            }
        }

        public void bindHeaderData(ProductAddonGroup item) {
            ctvAddOnGroupName.setText(item.getName());
        }

        class GenericTextWatcher implements TextWatcher {

            final String prefix = itemView.getContext().getResources().getString(R.string.currency_value);
            private final EditText view;

            private GenericTextWatcher(EditText view) {
                this.view = view;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().startsWith(prefix)) {
                    view.setText(prefix);
                    Selection.setSelection(view.getText(), view.getText().length());
                } else {
                    ProductAddonsList data = itemList.get(headerPosition).getAddon().get(itemPosition);
                    data.setPrice(editable.toString());
                }
            }
        }
    }
}