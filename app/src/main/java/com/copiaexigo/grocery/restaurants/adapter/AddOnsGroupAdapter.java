package com.copiaexigo.grocery.restaurants.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.model.AddonsGroups;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AddOnsGroupAdapter extends RecyclerView.Adapter<AddOnsGroupAdapter.MyViewHolder> {
    private List<AddonsGroups> list = new ArrayList<>();
    private final AddOnGroupClick addOnGroupClick;

    public AddOnsGroupAdapter(AddOnGroupClick addOnGroupClick) {
        this.addOnGroupClick = addOnGroupClick;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.addons_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AddOnsGroupAdapter.MyViewHolder holder, int position) {
        holder.bindData(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setItemList(List<AddonsGroups> addonsGroupsList) {
        this.list = addonsGroupsList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final ImageView closeImg;

        public MyViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.addons_name_txt);
            closeImg = itemView.findViewById(R.id.close_img);
        }

        public void bindData(AddonsGroups addon) {
            Context context = itemView.getContext();
            name.setText(addon.getName());
            closeImg.setOnClickListener(v -> {
                AlertDialog.Builder cancelAlert = new AlertDialog.Builder(context);
                cancelAlert.setTitle(context.getResources().getString(R.string.add_ons_group));
                cancelAlert.setMessage(context.getResources().getString(R.string.are_you_sure_want_to_delete_addon_group));
                cancelAlert.setPositiveButton(context.getResources().getString(R.string.okay), (dialog, whichButton) -> addOnGroupClick.itemDelete(addon));
                cancelAlert.setNegativeButton(context.getResources().getString(R.string.cancel), (dialog, whichButton) -> dialog.dismiss());
                cancelAlert.show();
            });
            name.setOnClickListener(v -> {
                addOnGroupClick.itemClick(addon);
            });

        }
    }

    public interface AddOnGroupClick {
        void itemClick(AddonsGroups addon);

        void itemDelete(AddonsGroups addon);
    }
}
