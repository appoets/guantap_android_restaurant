package com.copiaexigo.grocery.restaurants.model.addonsdata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ProductAddon implements Parcelable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("addon_id")
    private Integer addonId;
    @SerializedName("product_id")
    private Integer productId;
    @SerializedName("price")
    private Integer price;
    @SerializedName("addon_group_id")
    private Integer addonGroupId;
    private boolean isChecked = false;


    public ProductAddon(Integer id, Integer addonId, Integer productId, Integer price, Integer addonGroupId, boolean isChecked) {
        this.id = id;
        this.addonId = addonId;
        this.productId = productId;
        this.price = price;
        this.addonGroupId = addonGroupId;
        this.isChecked = isChecked;
    }

    protected ProductAddon(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            addonId = null;
        } else {
            addonId = in.readInt();
        }
        if (in.readByte() == 0) {
            productId = null;
        } else {
            productId = in.readInt();
        }
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readInt();
        }
        if (in.readByte() == 0) {
            addonGroupId = null;
        } else {
            addonGroupId = in.readInt();
        }
        isChecked = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (addonId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addonId);
        }
        if (productId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(productId);
        }
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(price);
        }
        if (addonGroupId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addonGroupId);
        }
        dest.writeByte((byte) (isChecked ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProductAddon> CREATOR = new Creator<ProductAddon>() {
        @Override
        public ProductAddon createFromParcel(Parcel in) {
            return new ProductAddon(in);
        }

        @Override
        public ProductAddon[] newArray(int size) {
            return new ProductAddon[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAddonId() {
        return addonId;
    }

    public void setAddonId(Integer addonId) {
        this.addonId = addonId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getPrice() {
        if (price == null)
            price = 0;
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getAddonGroupId() {
        return addonGroupId;
    }

    public void setAddonGroupId(Integer addonGroupId) {
        this.addonGroupId = addonGroupId;
    }


}