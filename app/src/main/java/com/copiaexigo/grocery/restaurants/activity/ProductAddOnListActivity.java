package com.copiaexigo.grocery.restaurants.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.adapter.ProductAddonGroupAdapter;
import com.copiaexigo.grocery.restaurants.helper.ConnectionHelper;
import com.copiaexigo.grocery.restaurants.helper.CustomDialog;
import com.copiaexigo.grocery.restaurants.model.ServerError;
import com.copiaexigo.grocery.restaurants.model.addonsdata.ProductAddon;
import com.copiaexigo.grocery.restaurants.model.addonsdata.ProductAddonGroup;
import com.copiaexigo.grocery.restaurants.model.addonsdata.ProductAddonsList;
import com.copiaexigo.grocery.restaurants.model.addonsdata.UpdatedAddons;
import com.copiaexigo.grocery.restaurants.network.ApiClient;
import com.copiaexigo.grocery.restaurants.network.ApiInterface;
import com.copiaexigo.grocery.restaurants.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductAddOnListActivity extends AppCompatActivity {

    @BindView(R.id.back_img)
    ImageView imgBack;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.rvAddOns)
    RecyclerView rvAddOns;
    @BindView(R.id.exAddons)
    ExpandableListView exAddons;

    private ConnectionHelper connectionHelper;
    private CustomDialog customDialog;
    private final ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private final ArrayList<UpdatedAddons> updatedAddons = new ArrayList<>();
    private ArrayList<ProductAddonGroup> listReceivedAddOns = new ArrayList<>();
    private ProductAddonGroupAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add_on_list);
        ButterKnife.bind(this);
        setUp();
    }

    private void setUp() {
        connectionHelper = new ConnectionHelper(this);
        customDialog = new CustomDialog(this);
        title.setText(R.string.select_addons);
        imgBack.setVisibility(View.VISIBLE);
        rvAddOns.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvAddOns.setHasFixedSize(true);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            listReceivedAddOns = bundle.getParcelableArrayList("addon");
        }
    }

    @OnClick({R.id.btnSave, R.id.back_img})
    public void onSave(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if (adapter != null) {
                    if (adapter.verifyPrice()) {
                        updatedAddons.clear();
                        ArrayList<ProductAddonGroup> itemList = adapter.getSelectAddOnList();
                        final String prefix = getResources().getString(R.string.currency_value);
                        for (int i = 0; i < itemList.size(); i++) {
                            ProductAddonGroup item = itemList.get(i);
                            for (ProductAddonsList addonsList : item.getAddon()) {
                                if (addonsList.getIsCheck()) {
                                    if (addonsList.getPrice().replace(prefix, "").isEmpty()) {
                                        Utils.displayMessage(this, getResources().getString(R.string.please_enter_price));
                                        break;
                                    }
                                    updatedAddons.add(new UpdatedAddons(addonsList.getId(),
                                            addonsList.getName(),
                                            addonsList.getPrice().replace(prefix, ""),
                                            addonsList.getAddonGroupId())
                                    );
                                }
                            }
                        }

                        if (listReceivedAddOns.size() > 0) {
                            showSuccessAndGoBack();
                        } else {
                            goBackToScreen();
                        }
                    }
                } else {
                    setResult(RESULT_OK);
                    finish();
                }
                break;
            case R.id.back_img:
                onBackPressed();
                break;
        }
    }

    private void showSuccessAndGoBack() {
        Utils.displayMessage(ProductAddOnListActivity.this, getResources().getString(R.string.addons_updated_successfully));
        new Handler().postDelayed(this::goBackToScreen, 1000);
    }


    private ProductAddon getEmptyAddon() {
        return new ProductAddon(0, 0, 0, 0, 0, true);
    }

    private void goBackToScreen() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("addon", updatedAddons);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (connectionHelper.isConnectingToInternet()) {
            getAddOnList();
        } else {
            Utils.displayMessage(this, getString(R.string.oops_no_internet));
        }
    }

    private void getAddOnList() {
        customDialog.show();

        Call<List<ProductAddonGroup>> call = apiInterface.getAddOnsGroupsList();
        call.enqueue(new Callback<List<ProductAddonGroup>>() {
            @Override
            public void onResponse(Call<List<ProductAddonGroup>> call, Response<List<ProductAddonGroup>> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setItemList(new ArrayList<>(response.body()), listReceivedAddOns);
                    } else {
                        setItemList(new ArrayList<>(), listReceivedAddOns);
                    }
                } else {
                    Gson gson = new Gson();
                    try {
                        ServerError serverError = gson.fromJson(response.errorBody().charStream(), ServerError.class);
                        Utils.displayMessage(ProductAddOnListActivity.this, serverError.getError());
                    } catch (JsonSyntaxException e) {
                        Utils.displayMessage(ProductAddOnListActivity.this, getString(R.string.something_went_wrong));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ProductAddonGroup>> call, Throwable t) {
                customDialog.dismiss();
                Utils.displayMessage(ProductAddOnListActivity.this, getString(R.string.something_went_wrong));
            }
        });
    }

    public void setItemList(ArrayList<ProductAddonGroup> items, ArrayList<ProductAddonGroup> selectedList) {
        ArrayList<ProductAddonGroup> itemList = new ArrayList<>();
        if (!selectedList.isEmpty()) {
            for (int i = 0; i < items.size(); i++) {
                ProductAddonGroup addonGroupi = items.get(i);
                for (int k = 0; k < addonGroupi.getAddon().size(); k++) {
                    ProductAddonsList productAddonsListi = addonGroupi.getAddon().get(k);
                    for (int j = 0; j < selectedList.size(); j++) {
                        ProductAddonGroup addonGroupj = selectedList.get(j);
                        for (ProductAddonsList productAddonsListj : addonGroupj.getAddon()) {
                            if (productAddonsListi.getId().equals(productAddonsListj.getId())) {
                                productAddonsListi.setChecked(true);
                                if (productAddonsListj.getAddonproducts() != null) {
                                    productAddonsListi.setPrice("" + productAddonsListj.getAddonproducts().getPrice());
                                }
                                addonGroupi.getAddon().set(k, productAddonsListi);
                            }
                        }
                    }
                }
                itemList.add(addonGroupi);
            }
        } else {
            for (int i = 0; i < items.size(); i++) {
                ProductAddonGroup addonGroupi = items.get(i);
                for (int k = 0; k < addonGroupi.getAddon().size(); k++) {
                    ProductAddonsList productAddonsListi = addonGroupi.getAddon().get(k);
                    productAddonsListi.setPrice("");
                    productAddonsListi.setChecked(false);
                    addonGroupi.getAddon().set(k, productAddonsListi);
                }
                itemList.add(addonGroupi);
            }
        }
        adapter = new ProductAddonGroupAdapter(this);
        rvAddOns.setAdapter(adapter);
        adapter.setItemList(itemList);
        runOnUiThread(() -> adapter.notifyDataSetChanged());

    }

}
