package com.copiaexigo.grocery.restaurants.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.activity.EditRestaurantActivity;
import com.copiaexigo.grocery.restaurants.activity.SplashActivity;
import com.copiaexigo.grocery.restaurants.adapter.SettingAdapter;
import com.copiaexigo.grocery.restaurants.controller.GetProfile;
import com.copiaexigo.grocery.restaurants.controller.ProfileListener;
import com.copiaexigo.grocery.restaurants.helper.ConnectionHelper;
import com.copiaexigo.grocery.restaurants.helper.CustomDialog;
import com.copiaexigo.grocery.restaurants.helper.SharedHelper;
import com.copiaexigo.grocery.restaurants.model.Profile;
import com.copiaexigo.grocery.restaurants.model.Setting;
import com.copiaexigo.grocery.restaurants.network.ApiClient;
import com.copiaexigo.grocery.restaurants.network.ApiInterface;
import com.copiaexigo.grocery.restaurants.utils.LocaleUtils;
import com.copiaexigo.grocery.restaurants.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SettingFragment extends Fragment implements ProfileListener, SettingAdapter.languageClick {

    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.setting_rv)
    RecyclerView settingRv;
    Unbinder unbinder;

    Context context;
    Activity activity;
    List<Setting> settingArrayList;
    SettingAdapter settingAdapter;
    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.profile_img)
    ImageView profileImg;

    @BindView(R.id.shop_name)
    TextView shop_name;

    @BindView(R.id.shop_cuisines)
    TextView shop_cuisines;

    @BindView(R.id.address)
    TextView address;

    ConnectionHelper connectionHelper;
    CustomDialog customDialog;

    @BindView(R.id.lnrProfile)
    LinearLayout lnrProfile;

    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        activity = getActivity();
        initViews();
        return view;
    }

    private void initViews() {
        connectionHelper = new ConnectionHelper(getActivity());
        customDialog = new CustomDialog(getActivity());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        title.setText(R.string.restaurant);
        settingArrayList = new ArrayList<>();
        settingArrayList.add(new Setting(getString(R.string.history), R.drawable.ic_timer_timing_tool));
        settingArrayList.add(new Setting(getString(R.string.edit_restaurant), R.drawable.ic_edit));
        settingArrayList.add(new Setting(getString(R.string.edit_timing), R.drawable.ic_edit_time));
        settingArrayList.add(new Setting(getString(R.string.deliveries), R.drawable.ic_delivery_truck));
        settingArrayList.add(new Setting(getString(R.string.chenge_language), R.drawable.ic_translte));
        settingArrayList.add(new Setting(getString(R.string.change_password), R.drawable.ic_padlock));
        settingArrayList.add(new Setting(getString(R.string.logout), R.drawable.logout));
        settingArrayList.add(new Setting(getString(R.string.delete_account), R.drawable.trash));

        settingRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        settingRv.setItemAnimator(new DefaultItemAnimator());
        settingRv.setHasFixedSize(true);
        settingAdapter = new SettingAdapter(settingArrayList, context, activity,this);
        settingRv.setAdapter(settingAdapter);
    }


    @OnClick({R.id.lnrProfile})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.lnrProfile:
                context.startActivity(new Intent(context, EditRestaurantActivity.class));
                break;
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (connectionHelper.isConnectingToInternet())
            getProfile();
        else
            Utils.displayMessage(getActivity(), getString(R.string.oops_no_internet));
    }

    private void getProfile() {
        if (connectionHelper.isConnectingToInternet()) {
            customDialog.show();
            new GetProfile(apiInterface, this);
        } else {
            Utils.displayMessage(getActivity(), getResources().getString(R.string.oops_no_internet));
        }
    }


    @Override
    public void onSuccess(Profile profile) {
        customDialog.dismiss();
        if (isAdded()) {
            Glide.with(getActivity()).load(profile.getAvatar())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_place_holder_image).error(R.drawable.ic_place_holder_image).dontAnimate()).into(profileImg);
        }
        shop_name.setText(profile.getName());

        if (profile.getCuisines().size() != 0) {
            if (profile.getCuisines().size() > 1)
                shop_cuisines.setText("Multi Cuisine");
            else {
                String cuisines = profile.getCuisines().get(0).getName();
                shop_cuisines.setText(cuisines);
            }
        }

        address.setText(profile.getMapsAddress());
    }

    @Override
    public void onFailure(String error) {
        customDialog.dismiss();
        Utils.displayMessage(getActivity(), getString(R.string.something_went_wrong));
    }

    @Override
    public void clickLanguage() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.language_dialog, null);
        alertDialog.setView(convertView);
        alertDialog.setCancelable(true);
        alertDialog.setTitle(R.string.chenge_language);
        final AlertDialog alert = alertDialog.create();
        final RadioGroup chooseLanguage = convertView.findViewById(R.id.choose_language);
        final RadioButton english = convertView.findViewById(R.id.english);
        final RadioButton german = convertView.findViewById(R.id.german);
        String dd = LocaleUtils.getLanguage(context);
        switch (dd) {
            case "es":
                german.setChecked(true);
                break;
            case "en":
            default:
                english.setChecked(true);
                break;
        }
        chooseLanguage.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.english:
                    setLanguage("English");
                    alert.dismiss();
                    break;
                case R.id.german:
                    setLanguage("Spanish");
                    alert.dismiss();
                    break;
            }
        });
        alert.show();
    }

    private void setLanguage(String value) {
        SharedHelper.putKey(requireActivity(), "language", value);
        switch (value) {
            case "Spanish":
                LocaleUtils.setLocale(requireActivity(), "es");
                break;
            case "English":
            default:
                LocaleUtils.setLocale(requireActivity(), "en");
                break;
        }
        SharedHelper.putKey(context, "language", value);
        Utils.displayMessage(getActivity(), getString(R.string.language_changed));
        startActivity(new Intent(getActivity(), SplashActivity.class));
        requireActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        requireActivity().finishAffinity();
    }

}
