package com.copiaexigo.grocery.restaurants.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class AddonsGroups implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("min_selection")
    @Expose
    private Integer minSelection;
    @SerializedName("max_selection")
    @Expose
    private Integer maxSelection;

    public AddonsGroups(Integer id, String name, Integer shopId, Integer minSelection, Integer maxSelection) {
        this.id = id;
        this.name = name;
        this.shopId = shopId;
        this.minSelection = minSelection;
        this.maxSelection = maxSelection;
    }

    protected AddonsGroups(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        if (in.readByte() == 0) {
            shopId = null;
        } else {
            shopId = in.readInt();
        }
        if (in.readByte() == 0) {
            minSelection = null;
        } else {
            minSelection = in.readInt();
        }
        if (in.readByte() == 0) {
            maxSelection = null;
        } else {
            maxSelection = in.readInt();
        }
    }

    public static final Creator<AddonsGroups> CREATOR = new Creator<AddonsGroups>() {
        @Override
        public AddonsGroups createFromParcel(Parcel in) {
            return new AddonsGroups(in);
        }

        @Override
        public AddonsGroups[] newArray(int size) {
            return new AddonsGroups[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getMinSelection() {
        return minSelection;
    }

    public void setMinSelection(Integer minSelection) {
        this.minSelection = minSelection;
    }

    public Integer getMaxSelection() {
        return maxSelection;
    }

    public void setMaxSelection(Integer maxSelection) {
        this.maxSelection = maxSelection;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(name);
        if (shopId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(shopId);
        }
        if (minSelection == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(minSelection);
        }
        if (maxSelection == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(maxSelection);
        }
    }

    @NotNull
    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddonsGroups that = (AddonsGroups) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(shopId, that.shopId) &&
                Objects.equals(minSelection, that.minSelection) &&
                Objects.equals(maxSelection, that.maxSelection);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, shopId, minSelection, maxSelection);
    }
}
