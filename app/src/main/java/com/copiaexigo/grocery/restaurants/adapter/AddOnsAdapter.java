package com.copiaexigo.grocery.restaurants.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.activity.AddAddOnsActivity;
import com.copiaexigo.grocery.restaurants.activity.AddOnsActivity;
import com.copiaexigo.grocery.restaurants.helper.GlobalData;
import com.copiaexigo.grocery.restaurants.model.Addon;

import java.util.List;

public class AddOnsAdapter extends RecyclerView.Adapter<AddOnsAdapter.MyViewHolder> {
    private final Context context;
    private Activity activity;
    private final List<Addon> list;

    public AddOnsAdapter(List<Addon> list, Context con) {
        this.list = list;
        this.context = con;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.addons_list_item, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Addon Addon = list.get(position);
        holder.name.setText(Addon.getName());
        holder.closeImg.setOnClickListener(v -> {
            AlertDialog.Builder cancelAlert = new AlertDialog.Builder(context);
            cancelAlert.setTitle(context.getResources().getString(R.string.add_ons));
            cancelAlert.setMessage(context.getResources().getString(R.string.are_you_sure_want_to_delete_addon));
            cancelAlert.setPositiveButton(context.getResources().getString(R.string.okay), (dialog, whichButton) -> ((AddOnsActivity) context).deleteAddon(list.get(position), position));
            cancelAlert.setNegativeButton(context.getResources().getString(R.string.cancel), (dialog, whichButton) -> dialog.dismiss());
            cancelAlert.show();
        });
        holder.name.setOnClickListener(v -> {
            GlobalData.selectedAddon = list.get(position);
            context.startActivity(new Intent(context, AddAddOnsActivity.class).putExtra("is_update", true));
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void add(Addon item, int position) {
        list.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(Addon item) {
        int position = list.indexOf(item);
        list.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final ImageView closeImg;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.addons_name_txt);
            closeImg = view.findViewById(R.id.close_img);
        }
    }
}
