package com.copiaexigo.grocery.restaurants.model.addonsdata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UpdatedAddons implements Parcelable {
    private Integer addonId;
    private String addonName;
    private String price;
    private Integer addonGroupId;

    public UpdatedAddons(Integer addonId, String addonName, String price, Integer addonGroupId) {
        this.addonId = addonId;
        this.addonName = addonName;
        this.price = price;
        this.addonGroupId = addonGroupId;
    }

    protected UpdatedAddons(Parcel in) {
        if (in.readByte() == 0) {
            addonId = null;
        } else {
            addonId = in.readInt();
        }
        if (in.readByte() == 0) {
            addonName = null;
        } else {
            addonName = in.readString();
        }
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readString();
        }
        if (in.readByte() == 0) {
            addonGroupId = null;
        } else {
            addonGroupId = in.readInt();
        }
    }

    public static final Creator<UpdatedAddons> CREATOR = new Creator<UpdatedAddons>() {
        @Override
        public UpdatedAddons createFromParcel(Parcel in) {
            return new UpdatedAddons(in);
        }

        @Override
        public UpdatedAddons[] newArray(int size) {
            return new UpdatedAddons[size];
        }
    };

    public Integer getAddonId() {
        return addonId;
    }

    public void setAddonId(Integer addonId) {
        this.addonId = addonId;
    }

    public String getAddonName() {
        return addonName;
    }

    public void setAddonName(String addonName) {
        this.addonName = addonName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getAddonGroupId() {
        return addonGroupId;
    }

    public void setAddonGroupId(Integer addonGroupId) {
        this.addonGroupId = addonGroupId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (addonId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addonId);
        }
        if (addonName == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(addonName);
        }
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(price);
        }
        if (addonGroupId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addonGroupId);
        }
    }
}
