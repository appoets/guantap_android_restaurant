package com.copiaexigo.grocery.restaurants.controller;

import android.util.Log;

import androidx.annotation.NonNull;

import com.copiaexigo.grocery.restaurants.application.MyApplication;
import com.copiaexigo.grocery.restaurants.helper.AppDeviceIDToken;
import com.copiaexigo.grocery.restaurants.helper.GlobalData;
import com.copiaexigo.grocery.restaurants.helper.SharedHelper;
import com.copiaexigo.grocery.restaurants.model.Profile;
import com.copiaexigo.grocery.restaurants.model.ServerError;
import com.copiaexigo.grocery.restaurants.network.ApiInterface;
import com.copiaexigo.grocery.restaurants.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Tamil on 3/16/2018.
 */

public class GetProfile {

    public GetProfile(ApiInterface apiInterface, final ProfileListener profileListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put("device_id", AppDeviceIDToken.getDeviceID(MyApplication.getInstance()));
        params.put("device_type", "Android");
        params.put("device_token", AppDeviceIDToken.getDeviceToken(MyApplication.getInstance()));
        Log.d("TAG", "AppDeviceIDToken===>" + AppDeviceIDToken.getDeviceToken(MyApplication.getInstance()));
        Log.d("getProfileMap",params.toString());
        Log.d("accessToken", GlobalData.accessToken);
        Call<Profile> call = apiInterface.getProfile(params);
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(@NonNull Call<Profile> call, @NonNull Response<Profile> response) {
                if (response.isSuccessful()) {
                    SharedHelper.putKey(MyApplication.getInstance(), Constants.PREF.PROFILE_ID, "" + response.body().getId());
                    SharedHelper.putKey(MyApplication.getInstance(), Constants.PREF.CURRENCY, "" + response.body().getCurrency());
                    profileListener.onSuccess(response.body());
                } else try {
                    assert response.errorBody() != null;
                    ServerError serverError = new Gson().fromJson(response.errorBody().charStream(), ServerError.class);
                    profileListener.onFailure(serverError.getError());
                } catch (JsonSyntaxException e) {
                    profileListener.onFailure("");
                }
            }

            @Override
            public void onFailure(@NonNull Call<Profile> call, @NonNull Throwable t) {
                profileListener.onFailure("");
            }
        });
    }
}
