package com.copiaexigo.grocery.restaurants.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.copiaexigo.grocery.restaurants.R;
import com.copiaexigo.grocery.restaurants.controller.GetProfile;
import com.copiaexigo.grocery.restaurants.controller.ProfileListener;
import com.copiaexigo.grocery.restaurants.helper.ConnectionHelper;
import com.copiaexigo.grocery.restaurants.helper.CustomDialog;
import com.copiaexigo.grocery.restaurants.helper.GlobalData;
import com.copiaexigo.grocery.restaurants.helper.SharedHelper;
import com.copiaexigo.grocery.restaurants.model.Profile;
import com.copiaexigo.grocery.restaurants.network.ApiClient;
import com.copiaexigo.grocery.restaurants.network.ApiInterface;
import com.copiaexigo.grocery.restaurants.utils.LocaleUtils;
import com.copiaexigo.grocery.restaurants.utils.Utils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

public class SplashActivity extends AppCompatActivity implements ProfileListener {

    private final ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    private final String TAG = "SplashActivity";
    private Context context;
    private Activity activity;
    private ConnectionHelper connectionHelper;
    private CustomDialog customDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FirebaseApp.initializeApp(this);
        context = SplashActivity.this;
        activity = SplashActivity.this;
        connectionHelper = new ConnectionHelper(context);
        customDialog = new CustomDialog(context);
        GlobalData.accessToken = SharedHelper.getKey(context, "access_token");

        getDeviceToken();
        String dd = SharedHelper.getKey(context, "language");
        switch (dd) {
            case "Spanish":
                LocaleUtils.setLocale(context, "es");
                break;
            case "English":
            default:
                LocaleUtils.setLocale(context, "en");
                break;
        }
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (SharedHelper.getKey(context, "logged").equalsIgnoreCase("true")
                    && SharedHelper.getKey(context, "logged") != null) {
                if (connectionHelper.isConnectingToInternet())
                    new GetProfile(apiInterface, SplashActivity.this);
                else
                    Utils.displayMessage(SplashActivity.this, getString(R.string.oops_no_internet));
            } else {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            }
        }, 3000);

    }


    @SuppressLint("HardwareIds")
    public void getDeviceToken() {
        try {
            FirebaseMessaging.getInstance().getToken().addOnSuccessListener(s -> {
                SharedHelper.putKey(context, "device_token", "" + s);
                Log.d(TAG, "getDeviceToken: " + s);
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    SharedHelper.putKey(context, "device_token", "" + task.getResult());
                    Log.d(TAG, "getDeviceToken: " + task.getResult());
                } else {
                    Log.d(TAG, "getDeviceToken: " + "Token Failed" + task.getException());
                }
            }).addOnFailureListener(e -> {
                Log.d(TAG, "addOnFailureListener" + e);
            });
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh");
        }
    }

    @Override
    public void onSuccess(Profile profile) {
        GlobalData.profile = profile;
        startActivity(new Intent(context, HomeActivity.class));
        finish();
    }

    @Override
    public void onFailure(String error) {
        customDialog.dismiss();
        if (error.isEmpty())
            Utils.displayMessage(activity, getString(R.string.something_went_wrong));
        else
            Utils.displayMessage(activity, error);

        SharedHelper.putKey(context, "logged", "false");
        startActivity(new Intent(SplashActivity.this, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }
}
