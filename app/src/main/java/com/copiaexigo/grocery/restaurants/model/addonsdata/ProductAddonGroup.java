
package com.copiaexigo.grocery.restaurants.model.addonsdata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ProductAddonGroup implements Parcelable {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("shop_id")
    private Integer shopId;
    @SerializedName("min_selection")
    private Integer minSelection;
    @SerializedName("max_selection")
    private Integer maxSelection;
    @SerializedName("addon")
    private List<ProductAddonsList> addon = new ArrayList<>();;
    private boolean isChecked;

    protected ProductAddonGroup(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
        if (in.readByte() == 0) {
            shopId = null;
        } else {
            shopId = in.readInt();
        }
        if (in.readByte() == 0) {
            minSelection = null;
        } else {
            minSelection = in.readInt();
        }
        if (in.readByte() == 0) {
            maxSelection = null;
        } else {
            maxSelection = in.readInt();
        }
        in.readTypedList(addon, ProductAddonsList.CREATOR);
        isChecked = in.readByte() != 0x00;
    }

    public static final Creator<ProductAddonGroup> CREATOR = new Creator<ProductAddonGroup>() {
        @Override
        public ProductAddonGroup createFromParcel(Parcel in) {
            return new ProductAddonGroup(in);
        }

        @Override
        public ProductAddonGroup[] newArray(int size) {
            return new ProductAddonGroup[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getMinSelection() {
        return minSelection;
    }

    public void setMinSelection(Integer minSelection) {
        this.minSelection = minSelection;
    }

    public Integer getMaxSelection() {
        return maxSelection;
    }

    public void setMaxSelection(Integer maxSelection) {
        this.maxSelection = maxSelection;
    }

    public List<ProductAddonsList> getAddon() {
        return addon;
    }

    public void setAddon(List<ProductAddonsList> addon) {
        this.addon = addon;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(name);
        if (shopId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(shopId);
        }
        if (minSelection == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(minSelection);
        }
        if (maxSelection == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(maxSelection);
        }
        dest.writeTypedList(addon);
        dest.writeByte((byte) (isChecked ? 0x01 : 0x00));
    }
}
