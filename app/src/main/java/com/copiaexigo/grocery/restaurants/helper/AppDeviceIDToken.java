package com.copiaexigo.grocery.restaurants.helper;

import android.annotation.SuppressLint;
import android.content.Context;

public class AppDeviceIDToken {

    @SuppressLint("HardwareIds")
    public static String getDeviceID(Context mContext) {
        String device_UDID = "COULD NOT GET DEVICE ID";
        try {
            device_UDID = android.provider.Settings.Secure.getString(mContext.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return device_UDID;
    }

    public static String getDeviceToken(Context mContext) {
        return SharedHelper.getKey(mContext, "device_token");
    }
}
